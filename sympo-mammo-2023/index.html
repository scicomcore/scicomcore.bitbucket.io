<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Enabling AI research, development and validation using the OPTIMAM Mammography Imaging Database (OMI-DB) — </title>
    <link rel="stylesheet" href="poster.css">
    <meta name="viewport" content="height=device-height, width=device-width, initial-scale=1">
    <!-- Based on a poster template from https://github.com/cpitclaudel/academic-poster-template -->

          <script type="text/javascript" id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    
          <link href="https://fonts.googleapis.com/css2?family=Fira+Sans+Condensed:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&amp;family=Ubuntu+Mono:ital,wght@0,400;0,700;1,400;1,700&amp;family=Yanone+Kaffeesatz:ital,wght@0,400;0,700;1,400;1,700&amp;family=Open+Sans:ital,wght@0,400;0,700;1,400;1,700&amp;display=swap" rel="stylesheet">
    
  </head>

  <body vocab="http://schema.org/" typeof="ScholarlyArticle">
    <header role="banner">
      <div>
        <h1 property="headline">Enabling AI research, development and validation using the OPTIMAM Mammography Imaging Database (OMI-DB)</h1>
                <address>
              <a property="author">D. Ward<sup>1</sup></a>
  <a property="author">L. M. Warren<sup>1</sup></a>
  <a property="author">Z. Zahoor<sup>1</sup></a>
  <a property="author">A. Joiner<sup>1</sup></a>
  <a property="author">A. Mackenzie<sup>2</sup></a>
  <a property="author">E. Lewis<sup>1</sup></a>
  <a property="author">K. C. Young<sup>2</sup></a>
  <a property="author">M. D. Halling-Brown<sup>1</sup></a>
<br />  <sup>1</sup><a property="sourceOrganization">Scientific Computing</a>
  <sup>2</sup><a property="sourceOrganization">National Coordinating Centre for the Physics of Mammography</a>
  </br>
  <a property="sourceOrganization">
   Medical Physics, 
   Royal Surrey County Hospital
  </a>
        </address>
        <span class="publication-info">
                      <span property="publisher">Symposium Mammographicum 2023</span>,
            <time pubdate property="datePublished" datetime="2020-09-08">June 2023</time>
                  </span>
      </div>
      <div>
        <a href="https://medphys.royalsurrey.nhs.uk/department/scientific-computing">
<img src="./images/institution-logo.png" alt="Scientific Computing">
</a>
      </div>

      <style type="text/css">
  html { font-size: 1.15rem }
</style>

    </header>

    <main property="articleBody">
      
  <article property="abstract" style="grid-column: span 5">
        <header><h3>Abstract</h3></header>

    <p>The development and validation of Artificial Intelligence (AI) to improve the outcomes of breast screening depends on the availability of very large, well-curated, representative databases<sup>1</sup>.</p>

    <p>The OPTIMAM Mammography Image Database (OMI-DB<sup>2</sup>) was created to provide an annotated dataset to facilitate R&D. Screening mammograms and associated clinical data have been collected from the NHS Breast Screening Programme (NHSBSP) for screen-detected cancers, interval cancers and large samples of routine-recall cases from UK screening centres since 2011. </p>

    <p>Collection into the database is ongoing, with each case monitored and updated with new information and further screening episodes.</p>

  </article>

  <article style="grid-column: span 5">
    <header class="highlight"><h3>OMI-DB: Composition</h3></header>

    <figure>
    <figcaption>OMI-DB comprises 7,905,727 processed/unprocessed mammograms from 540,771 cases. The table shows the total counts for specific case outcomes.</figcaption>
    <table border="0" class="dataframe">
  <thead>
    <tr style="text-align: left;">
      <th>Case Outcome</th>
      <th>Sites</th>
      <th>Cases</th>
      <th>Studies</th>
      <th>Images</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Screen Detected Cancer</td>
      <td>8</td>
      <td>18,585</td>
      <td>74,817</td>
      <td>410,015</td>
    </tr>
    <tr>
      <td>Benign</td>
      <td>8</td>
      <td>12,533</td>
      <td>52,619</td>
      <td>319,541</td>
    </tr>
    <tr>
      <td>Interval Cancer</td>
      <td>7</td>
      <td>3,479</td>
      <td>8,382</td>
      <td>43,390</td>
    </tr>
    <tr>
      <td>Normal</td>
      <td>8</td>
      <td>488,234</td>
      <td>1,350,518</td>
      <td>6,881,008</td>
    </tr>
  </tbody>
</table>    </figure>

    <div class="gallery">
    <figure>
    <img height="120px" src="./images/FFDM.png" alt="FFDM" />
    <figcaption>FFDM: 1,534,314 Studies</figcaption>
    </figure>

    <figure>
    <img height="120px" src="./images/annontatedFFDM.png" alt="FFDM2" />
    <figcaption>Annotated Cancers: 7,000+ Studies</figcaption>
    </figure>

    <figure>
    <img height="120px" src="./images/DBT.png" alt="DBT" />
    <figcaption>DBT: 8,021 Studies </figcaption>
    </figure>

    <figure>
    <img height="120px" src="./images/MRI.png" alt="MRI" />
    <figcaption>MRI: 6,454 Studies*<p style="font-size: x-small">*collection in progress</p></figcaption>
    </figure>

   </div>

  </article>

<article style="grid-column: span 6">
    <header><h3>Collection sites</h3></header>

    <b>OMI-DB is rapidly expanding to ensure data are representative of screening centres across the NHSBSP.</b>

    <figure>
    <img src="./images/gmap.png" alt="SMART Map" />
    <figcaption>The collection of medical-imaging and clinical data is continuous and easily configured through the Royal Surrey's Secure Medical-image Anonymisation Receiver for Trials (SMART) box. The <a href="https://www.google.com/maps/d/u/0/edit?mid=1Z35XwYmclS6IOULBOfgNa8Gy3IXa_Wo&usp=sharing">map</a> shows the SMART boxes actively running at eight UK collection sites (red pins), with five new sites being onboarded (blue pins).</figcaption>
    </figure>
</article>

<article style="grid-column: span 9; grid-gap: 0">

    <header><h3>Research, development and validation</h3></header>

    <p>OMI-DB has been used in a number of virtual clinical trials investigating breast cancer detection and evaluating cancer characteristics and breast density.</p> 

    <p>More recently, several studies have evaluated algorithms at different stages of development, from prototypes to commercial products.</p>

    <p>Growing, unseen validation subsets have been retained for independent model validation.</p>

    <p>Sequential normal screening mammograms can be analysed using quantitative imaging features, with a priori knowledge that some years later particular cases develop a malignancy.</p>

    <header class="highlight">
    <h3>Case study of use of OMI-DB: Artificial Intelligence in Mammography Screening<sup>3</sup> (AIMS)</h3>
    <h4 style="margin: 0; text-align: left"> An AI validation and clinical workflow integration study</h4>
    </header>

    <div class="split3x2">

    <div>
    <h5 style="text-align: center; color: #5f568f; margin: 0">Part A</h5>

    <div style="background-color: #e5e1fa">
    <ul>
      <li>Validation of an AI system using OMI-DB imaging from five sites</li>
      <li>Large scale retrospective validation involving representative datasets of 25,000 cases​</li>
    </ul>
    </div>
    </div>

    <div style="background-color: #f1eff9">
    <figure>
    <img width="100%" src="./images/aims-partA.svg" alt="AIMS-A" />
    </figure>
    </div>

    <div style="background-color: #e5e1fa">
    <h5 style="text-align: center">Study learnings</h4>
        <ul>
        <li>Is the AI system as accurate in this new cohort?</li>
        <li>Is it fair and equitable for all demographic/clinical groups?</li>
        <li>What would be the impact upon clinical workflows?</li>
        </ul>
    </div>

    <div>
    <h5 style="text-align: center; color: #1b656d; margin: 0">Part B</h5>

    <div style="background-color: #c3dbd4">
    <ul>
      <li>Reader study assessing impact of AI on outcomes after arbitration</li>
      <li>Panel to decide whether to recall based on inter-human disagreement and human-AI disagreement</li>
    </ul>
    </div>
    </div>

    <div style="background-color: #e8edec">
    <figure>
    <img src="./images/aims-partB.svg" alt="AIMS-B" />
    </figure>
    </div>

    <div style="background-color: #c3dbd4">
    <h5 style="text-align: center">Study learnings</h4>
        <ul>
          <li>Comparison of overall accuracy and fairness between both arms</li>
          <li>How AI affects various screening programme metrics of interest</li>
          <li>Human factors where specialists interact with an AI system</li>
          <li>Failure case analysis</li>
          <li>Detailed health economic assessment</li>
        </ul>
    </div>

  </article>

    <article style="grid-column: span 7">
    <header><h3>Enabling research through distribution</h3></header>

    <p>Since 2014, the database has been shared with over 70 groups, primarily for the development of machine-learning systems applicable to breast cancer detection and diagnosis.</p>

    <figure>
    <img src="./images/omi-db-collection-process.svg" alt="SMART Collection" />
    <figcaption>Overview of the image collection and sharing procedure. The OMI-DB comprises several relational databases, cloud storage systems and bespoke image-collection and distribution software. </figcaption>
    </figure>

    <header class="highlight"><h3>Access to the Database</h3></header>
    <p>OMI-DB was originally designed to support a programme grant called OPTIMAM2<sup>4</sup>, funded by CRUK, which evaluates how various factors affect cancer detection and diagnosis using mammographic images. 

    <p>Other research groups can <a href="https://medphys.royalsurrey.nhs.uk/omidb/how-to-get-access/">apply for access</a> online<sup>5</sup>; applications go through a well defined data-sharing process (as highlighted in the figure above).</p>


    <header><h3>Summary</h3></header>
    <p>A national-scale research database has been developed. The provision of processed/unprocessed images, its large size, the availability of NBSS data and expert-determined ground truth are essential for the safe development and validation of AI.</p>

    <p>The availability of sequential screening events and interval cancers opens up many artificial intelligence research applications, including whether an abnormality could have been detected earlier. </p>

   </article>

  <article style="grid-column: span 9; font-size: 80%">
    <header><h3>References</h3></header>
    <ol>
    <li>Debelee TG, Schwenker F, Ibenthal A, Yohannes D. <a href="https://doi.org/10.1007/s12530-019-09297-2">Survey of deep learning in breast cancer image analysis</a>. Evol Syst. 2020 Mar; 11:143–163.</li>
    <li>Halling-Brown MD, Warren LM, Ward D, Lewis E, Mackenzie A, Wallis MG, Wilkinson LS, Given-Wilson RM, McAvinchey R, Young KC. <a href="https://doi.org/10.1148/ryai.2020200103">OPTIMAM mammography image database: a large-scale resource of mammography images and clinical data </a>. Radiol Artif Intell. 2020 Nov 25;3(1):e200103.</li>
    <li>Imperial College London. <a href="https://www.imperial.ac.uk/global-health-innovation/what-we-do/research/data-science-and-analytics/artificial-intelligence/ai-in-mammography-screening-aims">AI in Mammography Screening (AIMS)</a>. 2023.</li>
    </li>
    <li>NCCPM, Royal Surrey. <a href="https://medphys.royalsurrey.nhs.uk/nccpm/?s=optimamimagedatabase"> OPTIMAM Image Database</a>. Research project homepage. 2023.</li>
      <li>Medical Physics, Royal Surrey. <a href="https://medphys.royalsurrey.nhs.uk/omidb/"> OMI-DB OPTIMAM Mammography Imaging </a>. OMI-DB homepage. 2023.</.li>
    </ol>
  </article>

  <article style="grid-column: span 7; font-size: 80%">
    <header><h3>Acknowledgements</h3></header>
    We would like to thank
    <div class="split2">
    <div>
    <ul>
    <li>CRUK for originally funding OMI-DB and management of it</li>
    <li>The participating screening sites for their continued involvement</li> 
    <li>The OPTIMAM data request committee for reviewing applications for access</li>
    </ul>
    </div>

    <div>
    <ul>
    <li>The AIMs team for allowing us to use their study as an example use-case</li>
    <li>The authors of the open poster template:<br><a style="font-size: small" href="https://github.com/cpitclaudel/academic-poster-template">github.com/cpitclaudel/academic-poster-template</a></br> on which this poster is based</li>
    </ul>
    </div>

    </div>
  </article>

    </main>

    <footer>
      <address class="monospace">  <a href="https://scicomcore.bitbucket.io/sympo-mammo-2023">https://scicomcore.bitbucket.io/sympo-mammo-2023</a>
</address>
      <address class="monospace">  <a href="mailto:dominic.ward1@nhs.net">dominic.ward1@nhs.net</a>
</address>
          </footer>
  </body>
</html>