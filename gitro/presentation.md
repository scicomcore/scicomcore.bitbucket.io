class: center, middle
# An Introduction to Git
## Dom Ward

---
# What is Git?

.center[![:scale 100%](./images/dropbox.png)]

---
# What is Git?

.center[![:scale 100%](./images/dropbox_steroids.png)]

---
# What is Git?
## Traditional cloud-sync workflow:

<div class="images">
    <a style="border-bottom: none">
    <img src="./images/doc.png" width=100px height=100px>
    <div class="caption">app.py</div>
    <div class="caption">Jan</div>
    </a>
</div>

---
# What is Git?
## Traditional cloud-sync workflow:

<div class="images">
    <a style="border-bottom: none">
    <img src="./images/doc.png" width=100px height=100px>
    <div class="caption">app.py</div>
    <div class="caption">Jan</div>
    </a>
    ⟶
    <a style="border-bottom: none">
    <img src="./images/doc.png" width=100px height=100px>
    <div class="caption">app.py</div>
    <div class="caption">Feb</div>
    </a>
</div>

---
# What is Git?
## Traditional cloud-sync workflow:

<div class="images">
    <a style="border-bottom: none">
    <img src="./images/doc.png" width=100px height=100px>
    <div class="caption">app.py</div>
    <div class="caption">Jan</div>
    </a>
    ⟶
    <a style="border-bottom: none">
    <img src="./images/doc.png" width=100px height=100px>
    <div class="caption">app.py</div>
    <div class="caption">Feb</div>
    </a>
    ⟶
    <a style="border-bottom: none">
    <img src="./images/doc.png" width=100px height=100px>
    <div class="caption">test_feature.py</div>
    <div class="caption">March</div>
    </a>
</div>

---
# What is Git?
## Traditional cloud-sync workflow:

<div class="images">
    <a style="border-bottom: none">
    <img src="./images/doc.png" width=100px height=100px>
    <div class="caption">app.py</div>
    <div class="caption">Jan</div>
    </a>
    ⟶
    <a style="border-bottom: none">
    <img src="./images/doc.png" width=100px height=100px>
    <div class="caption">app.py</div>
    <div class="caption">Feb</div>
    </a>
    ⟶
    <a style="border-bottom: none">
    <img src="./images/doc.png" width=100px height=100px>
    <div class="caption">test_feature.py</div>
    <div class="caption">March</div>
    </a>
    ⟶
    <a style="border-bottom: none">
    <img src="./images/doc.png" width=100px height=100px>
    <div class="caption">app.py</div>
    <div class="caption">April</div>
    </a>
</div>

---
# What is Git?
## Problems

- Workspace is contaminated with old files

    ```shell
    🐮 > ls
    app.py  test_feature.py
    ```
    (even worse if just saving versions locally)
- How do we review differences?
- How do we inspect project history?
- Versions are at the _file level_
- Easy to break project; how do we collaborate?
- Workflow is not well defined: exactly when is a file versioned?

---
<!-- https://michaelstepner.com/blog/git-vs-dropbox/ -->
# What is Git?

## Distributed version control

- Codebase and complete history stored on multiple computers/servers
- Records changes to files as commits

--

## How different from Dropbox?

- Versions a project, not just files
- Traceability: History is fully documented
- File differencing, e.g. how does _my project_ differ from last month?
- Independent work streams: try out new approaches without affecting others
- Powerful synchronisation workflows through branching and merging
- Git hosting services offer continuous integration to automate work when changes are made

---
# So what should I use it for?

--
# .green[YOUR LIFE]

--

.center[![:scale 30%](./images/al_gore.png)]

--

Anything text based should be version controlled:

.left[
- Source code
- Shell scripts
- Config files
- Documentation
- CSV data
- Papers, presentations, theses etc.
]

--

**Avoid committing large or auto-generated files**

---
# Command Line Interface

```bash
git <command> <arguments>
```

---
# Identify yourself!

```shell
git config --global user.name Dominic Ward
git config --global user.email dominic.ward1@nhs.net
git config --global user.username dward_nhs
```

(see the [git-config page](https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-config }) for more)

---
# Identify yourself!

`~/.gitconfig`

```shell
[user]
    name = Dominic Ward
	email = dom@deeuu.me
	username = deeuu
[includeIf "gitdir:~/git/work/"]
  path = .gitconfig-work
```
--
`~/.gitconfig-work`
```shell
[user]
    name = Dominic Ward
    email = dominic.ward1@nhs.net
    username = dward_nhs
```
--

yields

```shell
🐮 > cd ~/git/site/
🐮 > git config user.email
dom@deeuu.me
🐮 > cd ~/git/work/scream/rn-components/
🐮 > git config user.email
dominic.ward1@nhs.net
```
---
# Setting up a repository (repo)

- __Repo__ is a project versioned by git
- **Local repo** = where you work on your computer
- **Remote repo** = central repository stored elsewhere, e.g GitHub, BitBucket

--
.center[![:scale 80%](./images/remotes.png)]
.caption[.right[source: https://duzun.me/tips/git]]

---
# Setting up a repository (repo)

.center[
## RSCH Scientific Computing BitBucket


![](./images/scicom_bitbucket.png)
.caption[https://bitbucket.org/scicomcore/]
]

---
# Setting up a repository (repo)
## `git init` // New local repo

```shell
🐮 > mkdir ~/temp/my_new_repo && cd ~/temp/my_new_repo
🐮 > git init
Initialised empty Git repository in /home/dominic/temp/my_new_repo/.git/
🐮 > ls -a
.  ..  .git
```

- Used when starting from scratch
- Creates a `master` branch (not special, just the default)

---
# Setting up a repository (repo)
## `git clone <url>` // Remote ⟶ local

```shell
git clone https://dward_nhs@bitbucket.org/scicomcore/scicomcore.bitbucket.io.git
Cloning into 'scicomcore.bitbucket.io'...
remote: Counting objects: 91, done.
remote: Compressing objects: 100% (89/89), done.
remote: Total 91 (delta 9), reused 0 (delta 0)
Unpacking objects: 100% (91/91), done.
```
- Use to copy an _existing_ remote repo to your local machine
- Different network protocols supported, e.g. SSH
    - Only HTTPs when on local network @ RSCH
- Notice my BitBucket username (`dward_nhs`) in the URL above
    - Saves me from having to enter my username when interacting with the
        remote
    - Present for repos that you're a collaborator on

--
.footnote[
## 🎠
]

???
- Show on bitbucket

---
# Basic workflow

## `git status`

- Inspect the repo state
> “What's going on?”


--

## `git add`

- Add a change to the **staging area**
> “Add these files/changes to the repo”

--

## `git commit`

- Commit the changes in **staging area**
> “Save the agreed state of my project”


---
# Staging area

.center[
![:scale 70%](./images/staging_area.png)
.caption[[A box for crafting commits](https://dev.to/sublimegeek/git-staging-area-explained-like-im-five-1anh)]
]

--

- Atomic commits minimise impact to project state
- Try to group related changes into snapshots

--
.footnote[
## 🎠
]

???
Start demo now!
Recap when returning to slides


---
# Review new commands

## `git diff`

View changes at project or file level (can feed this two commit hashes)

--
## `git log`

View commit history for this branch

--

## `git commit -v`
Show diffs in editor when adding your commit message

--

## `git commit -am <message>`
Add __all__ tracked changes and commit with a message

---
# Syncing: Local ⟶ Remote

__Goal__: We want to update a central repo with our local changes

--
## `git remote -v`

List remote connections and include the URL

--
## `git remote add <name> <url>`
Add a remote `<name>` located at `<url>`

`<name>` is typically `origin` for the central repo, there's nothing special
about it!

--
## `git push <name> <branch>`
Write local state of branch `<branch>` to the remote repo `<name>`

.footnote[
Note: You will need to create the remote repository, e.g. on BitBucket, if it doesn't exist!
]

--
.footnote[
## 🎠
]

???
Demonstrate pushing

---
# Syncing
.center[![](./images/jason_tina/jt-0.png)]

---
# Syncing
.center[![](./images/jason_tina/jt-1.png)]

---
# Syncing
.center[![](./images/jason_tina/jt-2.png)]

---
# Syncing
.center[![](./images/jason_tina/jt-3.png)]

---
# Syncing
.center[![](./images/jason_tina/jt-4.png)]

---
# Syncing
.center[![](./images/jason_tina/jt-5.png)]

---
# Syncing
.center[![](./images/jason_tina/jt-6.png)]

---
# Merging

.center[`git pull` = `git fetch` + `git merge`]

`fetch` downloads branches, commits and files from the remote repo

`merge` brings in content from another branch ; may
result in a "merge commit"

--
.center[![](./images/ff-merge.png)]

???
- In the example, Jason hadn't made any changes since Tina's first `commit`
- Jason's `pull` first fetched Tina's changes from `origin`, updating the local `origin/master` branch
- The subsequent `merge` simply pointed `master` to the tip of `origin/master`
- This is called a __fast-forward merge__

---
# Merge conflicts

.center[![](./images/jason_tina/jt-7.png)]

---
# Merge conflicts

.center[![](./images/jason_tina/jt-8.png)]

---
# Merge conflicts

.center[![](./images/jason_tina/jt-9.png)]

---
# Merge conflicts

.center[![](./images/jason_tina/jt-10.png)]

---
# Merge conflicts

.center[![](./images/jason_tina/jt-11.png)]

---
# Merge conflicts

.center[![](./images/jason_tina/jt-12.png)]

--
.very-small-font[
- Divergence occurs when two branches have independent commits following a common ancestor
- Conflicts occur you merge two branches that modify the same locations of the same file in different ways
- Conflicts require manual intervention (`git` doesn't know which changes should take precedence)
]

--
.footnote[
## 🎠
]

???
Back to demo to show this!

---
# Merge conflicts

```python
<<<<<<< HEAD
def add(a, b):
    '''
    Returns the sum of `a` and `b`
    '''
    return a + b

def multiply(a, b):
    '''
    Returns the product of `a` and `b`
    '''
    return a * b
=======
def add(a, b): return a + b
def multiply(a, b): return a * b
def divide(a, b): return a / b
>>>>>>> 6dd5e166d19f6bbd6dcff14a36baf601ade484c4
```

--
.small-font[
.left[
.blue[`<<<<<<<` to `=======`]

changes in local branch

.blue[`HEAD`]

Reference to latest commit
]

.right[
.blue[`=======` to `>>>>>>`]

changes in other branch

.blue[`6dd5e1...`]

commit of other branch where conflict started
]
]

---
## `git pull`

.center[![](./images/jason_tina/jt-13.png)]

--
.small-font[
- No linear path to join the history of both `master` branches
    - can't do a fast-forward merge
- Superfluous "Merge commits"
]

---
## `git pull --rebase`

.center[![](./images/jason_tina/jt-14.png)]

--
.small-font[
- Changes the base of your branch, so it looks like you checked out from
    different commit
- Rebase says “Put my changes on top of theirs”
- Often preferred for the ability to establish a linear history
- Never rebase commits that have been pushed to a public repository
]

---
# Branches

.center[![:scale 75%](./images/branching.png)]

.small-font[
- An independent line of development

- Multiple branches facilitates more sophisticated workflows:
    - Experiment without breaking a stable version
    - Implement, review and test new features without affecting core code base
    - Isolate and clean history before merging
]

---
# Branches

### `git branch`
List all branches in your repo

--
### `git branch <name>`
Create a branch called `<name>`

--
### `git checkout <name>`
Checkout the branch called `<name>`

--
### `git checkout -b <name>`
Create and checkout a new branch called `<name>`

--
.footnote[
## 🎠
]

---
# Merging a branch

1. `git checkout <dest>`

    switch to the destination branch you want to merge into

2. `git merge <src>`

    merge the source branch into the current (`<dest>`) branch

--

- If branches have diverged and you want to avoid the merge commit:
    consider `git rebase <src>` to put changes on `<dest>` on top of `<src>`

--

- Merging can also be done via a pull-request (Git hosting service only):
	- Offers _Merge Commit_, _Fast-Forward_ and _Squash_ strategies

--

### Cleaning up:
.left[
.small-font[`git branch -d <name>`

Delete local branch called `<name>`
]
]

.right[
.small-font[`git push origin --delete <name>`

Delete remote branch called `<name>`
]
]

---

# Undoing commits

`git reset --mixed HEAD~1`

Undo the last commit, but leave the changes in the working tree but not on the
index (will need to `add`).

`git reset --soft HEAD~1`

Undo the last commit, but leave the changes in the working tree and keep them
on the index.

`git reset --hard HEAD~1`

Undo the last commit and lose *all* uncommitted changes and those introduced in
the last commit. **Danger!**

.footnote[See the [Atlassian tutorial](https://www.atlassian.com/git/tutorials/undoing-changes/git-reset)]

---
# Thanks!

## .center[https://bitbucket.org/scicomcore/docs-gitro]

## Useful Resources

- [Great intro talk](https://www.youtube.com/watch?v=MJUJ4wbFm_A&t=2008s)
- [Atlassian](https://www.atlassian.com/git/tutorials/setting-up-a-repository)
- [Pull basics](https://www.atlassian.com/git/tutorials/syncing/git-pull)
- [Staging Area](https://dev.to/sublimegeek/git-staging-area-explained-like-im-five-1anh)
- [Cheatsheet](https://mukul-rathi.github.io/git-beginner-cheatsheet/)
- [Tips & Notes](https://duzun.me/tips/git)
- [Branches in a Nutshell](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)
- [Overview of merging](https://dev.to/neshaz/how-to-use-git-merge-the-correctway-25pd)
- [Branches and merging](https://www.atlassian.com/git/tutorials/using-branches/git-merge)
- [What is a fast-forward merge](https://sandofsky.com/images/fast_forward.pdf)
