class: center
# Subjective and Objective Performance Measurement of Singing-Voice Separation

### Dominic Ward, Hagen Wierstorf, Russell D. Mason
### Emad M. Grais, Mark D. Plumbley

### Musical Audio Repurposing using Source Separation

<br></br>
{ https://cvssp.github.io/perceptual-study-source-separation/ }

.bottom[![:scale 40%](./images/UOS.png)]

---
## Musical source separation

- State-of-the-art approach make use of DNNs to learn and predict:
    - Ideal time-frequency masks
    - Source spectrograms directly

--

- Perception of musical source separation affected by:

    - **Distortions**: Amplitude modulation, abrupt dropouts
    - **Artefacts**: Musical noise, pulses, clicks
    - **Interference**: Presence of unwanted instruments

<audio id='mA' src="./audio/UHL3_mixture.wav" type="audio/wav"></audio>
<audio id='vA' src="./audio/UHL3_vocals.wav" type="audio/wav"></audio>

<audio id='mB' src="./audio/KON_mixture.wav" type="audio/wav"></audio>
<audio id='vB' src="./audio/KON_vocals.wav" type="audio/wav"></audio>

<audio id='mC' src="./audio/DUR_mixture.wav" type="audio/wav"></audio>
<audio id='vC' src="./audio/DUR_vocals.wav" type="audio/wav"></audio>

| Mixture | Separated Vocal |
|---------|-----------------|
|<button onclick="play('mA')">A</button>|<button onclick="play('vA')">A</button>|
|<button onclick="play('mB')">B</button>|<button onclick="play('vB')">B</button>|
|<button onclick="play('mC')">C</button>|<button onclick="play('vC')">C</button>|


.caption[.center[{ http://sisec17.audiolabs-erlangen.de }]]

???

- Signal Separation Evaluation Campaign
- Source separation challenge for community

---
## Objective evaluation

Two performance evaluation toolkits based on distortion decomposition:

$$\hat{S} - S = e\_{\text{artefacts}} + e\_{\text{interference}} + e\_{\text{target}}$$

--

### BSS Eval.red[*]
Computes energy ratios from estimated error signals:

- .blue[SAR]: Source-to-Artefacts Ratio

- .blue[SIR]: Source-to-Interference Ratio

(Ratios in dB, higher = better performance)

.footnote[
.red[*Vincent et al. (2006) { https://doi.org/10.1109/tasl.2011.2109381 }]
]

---
## Objective evaluation

Two performance evaluation toolkits based on distortion decomposition:

$$\hat{S} - S = e\_{\text{artefacts}} + e\_{\text{interference}} + e\_{\text{target}}$$

### PEASS.red[*]
PEMO-Q auditory model to assess error saliency and maps output to quality scale using subjective data:

- .blue[APS]: Artefacts-related Perceptual Score
- .blue[IPS]: Interference-related Perceptual Score

(Score $\in (0, 100)$, higher = better performance)

.footnote[
.red[*Emiya et al. (2012) { https://doi.org/10.1109/tasl.2011.2109381 }]
]

???
- Each distortion has an associated metric
- Our hypothesis was to test if SIR and SAR correlate with interference and sound quality

---
class: middle
## Goals of the experiment

### 1. Does PEASS generalise to new singing-voice separation algorithms?.red[*]

### 2. Further investigate predictive capability of BSS Eval.blue[*]

.footnote[

.red[
*Original study: Emiya et al. (2012)
{ https://doi.org/10.1109/tasl.2011.2109381 }
]
.blue[
*Inconclusive: Gupta et al. (2015)
{ https://doi.org/10.1109/waspaa.2015.7336923 }
]
]

???
Decided to focus on singing-voice:
- More algorithms
- PEASS was originally fit to speech/singing-voice (70%)
- Easier for listeners

- Most previous studies assess BSS Eval through correlation with MOS if MUSHRA-like experiments
- Evidence that BSS Eval correlates with perception when applied to speech/singing-voice
- Some uncertainty as to whether BSS Eval generalises to other sources/algorithms (Cano et al)

---
## Methodology: Perceptual attributes

### Task 1: Sound quality

> Sound quality relates to the amount of artefacts or distortions that you can perceive.
> These can be heard as tone-like additions, abrupt changes in loudness, or missing parts of the audio.

--

Why didn't you ask for **target preservation** and **addition of artefacts**?.red[*]
- Avoid misinterpretation of the quality scale.blue[*]

.footnote[

.red[*Emiya et al. (2011) { https://doi.org/10.1109/tasl.2011.2109381 }]

.blue[*Cartwright et al. (2016) { https://doi.org/10.1109/icassp.2016.7471749 }]
]

???

- Time: We wanted as many subjects and stimuli as possible.
- Intuitive, more general quality scale (difficult to break quality down into other characteristics)
--

### Task 2: Interference

> Interference describes the loudness of the instruments compared to the
> loudness of the vocals. For example, ‘strong interference’ indicates a strong
> contribution from other instruments, whereas ‘no interference’ means that you
> can only hear the vocals.
> Interference does not include artefacts or distortions that you may perceive.

---
## Methodology
### Stimuli
- Source material from the MUS(ic) task of **SiSEC 2016**.red[*]:
    - 23 different participants (algorithms)
    - 50 test songs
    - Tasked with separation of **bass**, **drums**, **other** and **vocals**

.footnote[.red[*{ http://sisec17.audiolabs-erlangen.de/#/results/1/4/2 }]]

--
- Both listening tasks involved the same:
    - 16 songs, using **vocals** as the target stem
    - 5 algorithms per song (21 in total)
    - Synthesised our own anchors
    - All stimuli based on 7 second loudness matched segments

--
### Subjects
- 18 lab (CVSSP audio booth)
- Six listeners with audio engineering background participated online

???
- Signal Separation Evaluation Campaign

---
## Methodology

### Procedure: Sound Quality

.center[![:scale 100%](./images/interface.png)]
.caption[
100 point scale, where
- 0 = worse quality
- 100 = Same quality
]

???
- Headphone reproduction
- MUSHRA inspired interface
- Two rating tasks: Sound quality; Interference
- Reference always the vocal
- Five algorithms per page
- Hidden reference
- Two hidden anchors used in all trials:
    - Interference anchor $A_I$: The original mixture 
    - Sound quality anchor $A_Q$: Lowpass filtered + artefacts

---
## Methodology

### Procedure: Interference

.center[![:scale 100%](./images/interface_interference.png)]

.caption[
100 point scale, where
- 0 = Strong interference
- 100 = No interference
]



???
- Headphone reproduction
- MUSHRA inspired interface
- Two rating tasks: Sound quality; Interference
- Reference always the vocal
- Five algorithms per page
- Hidden reference
- Two hidden anchors used in all trials:
    - Interference anchor $A_I$: The original mixture 
    - Sound quality anchor $A_Q$: Lowpass filtered + artefacts

---
## Subjective data

<audio id='ref' src="./audio/ref.flac" type="audio/flac"></audio>
<audio id='quality' src="./audio/Quality.flac" type="audio/flac"></audio>
<audio id='interferer' src="./audio/Interferer.flac" type="audio/flac"></audio>

<div class='button-row'>
<button onclick="play('ref')">Ref</button> 
<button onclick="play('quality')">Quality Anchor</button>
<button onclick="play('interferer')">Interference Anchor</button>
</div>

.center[
![:scale 100%](./images/swarmplot_hidden_sounds_pres.png)
.caption[
Bee swarm plot showing the ratings of the hidden sound quality
and interference anchors and the hidden reference in both
tasks (higher score is better quality / less interference).
]
]

???
- Plot of all ratings for hidden stimuli
- Higher rating = better quality / less interference
- Start on reference, describing both tasks

---
## Objective metrics: within-song Spearman correlations

.center[
![:scale 75%](./images/boxplot_pres.png)
.caption[Boxplots of the subjective ratings for 1 trial of the quality task]
]


---
## Objective metrics: within-song Spearman correlations

.center[
![:scale 75%](./images/boxplot_with_predictions_pres.png)
.caption[Boxplots of the subjective ratings for 1 trial of the quality task]
]

--
- Important to remove hidden stimuli because:
    - Considered extreme values in subjective and objective ratings
    - Easy to predict, can inflate correlations

???
- We used median of ratings for all correlations

---
## Objective metrics: within-song Spearman correlations

.center[
![:scale 75%](./images/spearman_boxplot.png)
.caption[
Bee swarm plots (with boxplots underlaid) of the within-song Spearman correlations.]
]

???
- Quality:
    - APS rank correlations highest, tight across-song variance

- Interference:
    - SIR rank correlations highest, tight across-song variance

---
## Objective metrics: pooled Pearson correlations

.center[
### Sound Quality
![](./images/quality_pred_vs_subjective.png)
.caption[
Pearson correlation between medians of all 80 test sounds pooled from 16 trials
and metric predictions.
]
]

???
- Y shows fitted score after linear regression
- X shows median of subjective ratings per segment
- For SAR, no real trend above 20, curvilinear

---
## Objective metrics: pooled Pearson correlations

.center[
### Interference
![](./images/interference_pred_vs_subjective.png)
.caption[
Pearson correlation between medians of all 80 test sounds pooled from 16 trials
and metric predictions.
]
]

---
## Observations and reflections

### A few subjects ignored the accompaniment when judging sound quality:

- May have been due to familiarisation stage or definition

- Also consider style of the experiment (reference is the vocal)

- Two subjects repeated the experiment to judge overall quality

--

.blue[Potential solution]

- Emphasise **overall sound quality** with further audio examples

- Further analysis to investigate differences between subjects

---
## Observations and reflections

### Sometimes difficult to judge quality of the mixture compared to the vocal

--

.blue[Potential solution]

Supplement the reference with the original mixture.red[*]

.center[
![](./images/peass_mushra.png)
]

.footnote[
.red[*Emiya et al. (2011) { https://doi.org/10.1109/tasl.2011.2109381 }]
]

---
## Observations and reflections

### Using the original mixture as the interference anchor $A_I$ does not provide fixed quality across MUSHRA pages

- Loudness balance depends on the mix
- May lead to different positioning of systems under test on the quality scale

--

.blue[Potential solution]

$A_I~$ = Set vocal to be around -10 LU below mix loudness
.center[![:scale 70%](./images/relative_loudness.png)
.caption[
.red[*Ward et al. (2017), Fig 1 { http://epubs.surrey.ac.uk/id/eprint/841966 }]
]
]

---
## Previous work

#### Emiya et al. (2012) { https://doi.org/10.1109/tasl.2011.2109381 }
Found PEASS better mapped to subjective scores than BSS Eval

#### Gupta et al. (2015) { https://doi.org/10.1109/waspaa.2015.7336923 }
Some evidence that SIR and SAR correlate with vocal/accompaniment intelligibility

#### Cano et al. (2016) { https://doi.org/10.1109/eusipco.2016.7760550 } 
BSS Eval & PEASS weak for harmonic-percussive separation algorithms

#### Cartwright et al. (2016) { https://doi.org/10.1109/icassp.2016.7471749 }
SAR/SIR positive correlations but lower than web population

#### Simpson et al. (2017)  { https://doi.org/10.1007/978-3-319-53547-0_21 }
Found high within-song correlations of 0.91 for SAR (similarity) and SIR (interference)

---
## Objective evaluation

1. BSS Eval: Blind Source Separation Evaluation toolbox.red[*]
2. PEASS: Perceptual Evaluation methods for Audio Source Separation.blue[*]

Both approaches based on distortion decomposition:

$$\hat{S} - S = e\_{\text{artefacts}} + e\_{\text{interference}} + e\_{\text{target}}$$

where
- $\hat{S}$: Estimated source 
- $S$: Target source
- $e\_{\text{target}}$: Filtering/spatial distortion of the target source
- $e\_{\text{artefacts}}$: Artefacts, e.g. musical noise
- $e\_{\text{interference}}$: Unwanted sources (leakage)

.footnote[
.red[
*Vincent et al. (2006)
{ https://doi.org/10.1109/tasl.2011.2109381 }
]

.blue[
*Emiya et al. (2012)
{ https://doi.org/10.1109/tasl.2011.2109381 }
]
]
